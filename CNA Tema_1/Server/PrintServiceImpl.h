#pragma once

#include "..\Generated\PrintName.grpc.pb.h"

class PrintServiceImpl final : public PrintNameService::Service
{
public:
	PrintServiceImpl() {};
	::grpc::Status Print(::grpc::ServerContext* context, const ::PrintRequest* request, ::PrintResponse* response) override;
};

